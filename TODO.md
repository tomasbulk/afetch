# afetch

- [x] username - syscall getuid && /etc/passwd
- [x] hostname - syscall uname
- [x] os - /etc/os-release
- [x] kernel -  syscall uname
- [x] cpu - cpuid
- [x] uptime - /proc/uptime
- [x] shell - /proc/$$/cmdline && /proc/$$/exe (symlink)

- [x] commandline parameters - bunny | no-ascii
- [x] basename(const char \*name)
- [x] implement docopt
- [x] implement random eyes

- [x] format
