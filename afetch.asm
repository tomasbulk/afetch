;------------------------------------------------------------------------------
;
;  Name:     afetch
;  Author:   Tomas Bulk
;  Purpose:  Simple fetch utility written in i386 asssembly
;
;  Compile:  nasm -f elf32 -o afetch.o afetch.asm
;           ld -m elf_i386 -o afetch afetch.o
;
;------------------------------------------------------------------------------

%define _UTSNAME_LENGTH  65

section .data

  ANSI_RESET    db  0x1b,"[0m",0x00
  ANSI_BOLD     db  0x1b,"[1m",0x00
  ANSI_ITALIC   db  0x1b,"[3m",0x00
  ANSI_PURPLE   db  0x1b,"[35m",0x00
  ANSI_BLUE     db  0x1b,"[34m",0x00
  ANSI_RED      db  0x1b,"[31m",0x00
  NEWLINE       db  0x0a,0x00

  at_sign       db  '@',0x00
  no_ascii      db  "--no-ascii",0x00

  bunny_00      db  '            ',0x00
  bunny_01      db  0x1b,'[37;1m  (\ /)     ',0x00
  bunny_02      db  0x1b,'[37;1m  ( ',0x00
  bunny_03      db  ')    ',0x00
  bunny_04      db  0x1b,'[37;1m  c(")(")   ',0x00

  eyes          db  '. .',0x00,'^ ^',0x00,'- -',0x00,\
                    '~ ~',0x00,'* *',0x00,'-.-',0x00,\
                    '~.~',0x00,'*.*',0x00,'0.0',0x00,\
                    '0 0',0x00,'o o',0x00, 'o.o',0x00,'^.^',0x00

  os_out00      db  0x1b,"[1;35mos      ",0x1b,"[0m",0x00
  utsname_out02 db  0x1b,"[1;35mkernel  ",0x1b,"[0m",0x00
  cpuinfo_out00 db  0x1b,"[1;35mcpu     ",0x1b,"[0m",0x00
  shell_out00   db  0x1b,"[1;35mshell   ",0x1b,"[0m",0x00
  uptime_out00  db  0x1b,"[1;35muptime  ",0x1b,"[0m",0x00

  help_out00    db  "Usage: ",0x00
  help_out01    db  " [--no-ascii]",0x0a,0x00
  help_out02    db  0x09,"--no-ascii      Omit ascii art.",0x0a,0x0a,0x00


section .bss

  eyeindex  resd  0x01

  filename  resb  0x10

  os        resb  0x40
  shell     resb  0x30
  uptime    resb  0x30
  cpu_tmp   resb  0x30
  cpu_info  resb  0x30
  uid       resd  0x02
  username  resb  0x40

  utsname:
    sysname   resb  0x41
    nodename  resb  0x41
    release   resb  0x41
    version   resb  0x41
    machine   resb  0x41
    _padding  resb  0x03


section .text
  global _start

getrandom:
  ; int getrandom(const int min, const int max)
  push  ebp
  mov   ebp, esp

  ; int rnum  -> DWORD [ebp - 0x08]
  ; int fd    -> DWORD [ebp - 0x04]
  sub   esp, 0x08

  ; open /dev/urandom
  push  0x00        ; NULL
  push  0x6d6f646e  ; "ndom"
  push  0x6172752f  ; "/ura"
  push  0x7665642f  ; "/dev"
  mov   eax, 0x05
  mov   ebx, esp
  xor   ecx, ecx
  xor   edx, edx
  int   0x80
  mov   DWORD [ebp - 0x04], eax
  add   esp, 0x10
  
  ; read one byte
  mov   eax, 0x03
  mov   ebx, DWORD [ebp - 0x04]
  lea   ecx, [ebp - 0x08]
  mov   edx, 0x01
  int   0x80

  ; close /dev/urandom
  mov   eax, 0x06
  mov   ebx, DWORD [ebp - 0x04]
  int   0x80

  ; make into range
  mov   eax, DWORD [ebp - 0x08]
  mov   ebx, DWORD [ebp + 0x0c]
  inc   ebx,
  sub   ebx, DWORD [ebp + 0x08]
  cdq
  idiv  ebx
  add   edx, DWORD [ebp + 0x08]
  mov   DWORD [ebp - 0x08], edx

  mov   eax, DWORD [ebp - 0x08]
  mov   esp, ebp
  pop   ebp
  ret


basename:
  ; void basename(const char *path, char* buf)
  push  ebp
  mov   ebp, esp

  ; copy esi to edi
  mov   esi, DWORD [ebp + 0x08]
  mov   edi, DWORD [ebp + 0x0c]
  cld
  basename_loop00_top:
    movsb
    cmp   BYTE [esi-1], 0x00
    je    basename_loop00_end
    cmp   BYTE [esi-1], '/'
    jne   basename_loop00_top
    mov   edi, DWORD[ebp + 0x0c]
    jmp   basename_loop00_top
  basename_loop00_end:

  mov   esp, ebp
  pop   ebp
  ret


atoui:
  ; unsigned int atoui(char *str)
  push  ebp
  mov   ebp, esp

  ; int total  -> [ebp - 0x0c]
  ; int place  -> [ebp - 0x08]
  ; int len  -> [ebp - 0x04]
  sub   esp, 0x0c

  ; get amount of non-zero bytes
  mov   DWORD [ebp - 0x04], 0x00
  atoui_loop00_top:

    mov   esi, DWORD [ebp + 0x08]
    mov   edi, DWORD [ebp - 0x04]

    cmp   BYTE [esi + edi], 0x00
    je    atoui_loop00_end

  inc   DWORD [ebp - 0x04]
  jmp   atoui_loop00_top
  atoui_loop00_end:

  ; convert
  mov   DWORD [ebp - 0x0c], 0x00
  mov   DWORD [ebp - 0x08], 0x01
  dec   DWORD [ebp - 0x04]
  atoui_loop01_top:
  cmp   DWORD [ebp - 0x04], 0x00
  jl    atoui_loop01_end
  mov   esi, DWORD [ebp + 0x08]
  mov   edi, DWORD [ebp - 0x04]

    xor   eax, eax
    mov   al, BYTE [esi + edi]
    sub   al, 0x30
    mov   ebx, DWORD [ebp - 0x08]
    mul   ebx

    add   DWORD [ebp - 0x0c], eax

  dec   DWORD [ebp - 0x04]
  mov   eax, DWORD [ebp - 0x08]
  mov   ebx, 0x0a
  mul   ebx
  mov   DWORD [ebp - 0x08], eax
  jmp   atoui_loop01_top
  atoui_loop01_end:

  mov   eax, DWORD [ebp - 0x0c]
  mov   esp, ebp
  pop   ebp
  ret


getuptime:
  ; void getuptime(char *buf)
  push  ebp
  mov   ebp, esp

  ; int days      -> [ebp - 0x24]
  ; int hours     -> [ebp - 0x20]
  ; int minutes   -> [ebp - 0x1c]
  ; int dirtytime -> [ebp - 0x18]
  ; strbuf[16]    -> [ebp - 0x14] -> [ebp - 0x05]
  ; int fd        -> [ebp - 0x04]
  sub   esp, 0x24

  ; open /proc/uptime
  mov   eax, 0x05
  push  0x00
  push  0x656d6974  ; push "time"
  push  0x70752f63  ; push "c/up"
  push  0x6f72702f  ; push "/pro"
  mov   ebx, esp    ; ebx now has the address of "/proc/uptime"
  xor   ecx, ecx
  xor   edx, edx
  int   0x80
  mov   DWORD [ebp - 0x04], eax
  add   esp, 0x10

  ; read until the '.', store in buffer
  lea   esi, [ebp - 0x14]
  mov   edi, 0xffffffff
  getuptime_loop00_top:
  inc   edi
  
    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [esi + edi]
    mov   edx, 0x01
    int   0x80
  
  cmp   BYTE [esi + edi], '.'
  jne   getuptime_loop00_top
  mov   BYTE [esi + edi], 0

  ; dirtytime = atoui(strbuf)
  lea   esi, [ebp - 0x14]
  push  esi
  call  atoui
  add   esp, 0x04
  mov   DWORD [ebp - 0x18], eax

  ; calculate days
  mov   eax, DWORD [ebp - 0x18]
  xor   edx, edx
  mov   ebx, 0x15180
  div   ebx
  mov   DWORD [ebp - 0x24], eax

  ; calulate hours
  mov   eax, edx
  xor   edx, edx
  mov   ebx, 0xe10
  div   ebx,
  mov   DWORD [ebp - 0x20], eax

  ; calculate minutes
  mov   eax, edx
  xor   edx, edx
  mov   ebx, 0x3c
  div   ebx
  mov   DWORD [ebp - 0x1c], eax

  ; concatinate strings into buf
  mov   edi, DWORD [ebp + 0x08]

  getuptime_days:
  cmp   DWORD [ebp - 0x24], 0x00
  je    getuptime_hours

  ; uitoa(days, str_buf)
  push  edi
  lea   eax, [ebp - 0x14]
  push  eax
  push  DWORD [ebp - 0x24]
  call  uitoa
  add   esp, 0x08
  pop   edi

  ; loop till 0x00, copy days to buf
  lea   esi, [ebp - 0x14]
  getuptime_loop01_top:
    movsb
    cmp   BYTE [esi], 0x00
    jne   getuptime_loop01_top

  ; push " days " on the stack
  push  0x00        ; push NULL
  push  0x00002073  ; push "s "
  push  0x79616420  ; push " day"
  mov   esi, esp    ; esi now holds the address to " days "
  ; loop till 0x00, copy " days " to buf
  getuptime_loop02_top:
    movsb
    cmp   DWORD [esi], 0x00
    jne   getuptime_loop02_top
  add   esp, 0x0c

  getuptime_hours:
  cmp   DWORD [ebp - 0x20], 0x00
  je    getuptime_minutes

  ; uitoa(hours, str_buf)
  push  edi
  lea   eax, [ebp - 0x14]
  push  eax
  push  DWORD [ebp - 0x20]
  call  uitoa
  add   esp, 0x08
  pop   edi

  ; loop till 0x00, copy hours to buf
  lea   esi, [ebp - 0x14]
  getuptime_loop03_top:
    movsb
    cmp   BYTE [esi], 0x00
    jne   getuptime_loop03_top
  
  ; push " hours " on the stack
  push  0x00        ; push NULL
  push  0x00207372  ; push "rs "
  push  0x756f6820  ; push " hou"
  mov   esi, esp
  ; loop till 0x00, copy " hours " to buf
  getuptime_loop04_top:
    movsb
    cmp   DWORD [esi], 0x00
    jne   getuptime_loop04_top
  add   esp, 0x0c

  getuptime_minutes:
  cmp   DWORD [ebp - 0x18], 0x00
  je    atoui_end

  ; uitoa(hours, str_buf)
  push  edi
  lea   eax, [ebp - 0x14]
  push  eax
  push  DWORD [ebp - 0x1c]
  call  uitoa
  add   esp, 0x08
  pop   edi

  ; loop till 0x00, minutes to buf
  lea   esi, [ebp - 0x14]
  getuptime_loop05_top:
    movsb
    cmp   BYTE [esi], 0x00
    jne   getuptime_loop05_top
  
  ; push " minutes " onto the stack
  push  0x00        ; push NULL
  push  0x00000020  ; push " "
  push  0x73657475  ; push "utes"
  push  0x6e696d20  ; push " min"
  mov   esi, esp    ; esi now holds the address to " minutes "
  ; loop till 0x00, copy " minutes " to buf
  getuptime_loop06_top:
    movsb
    cmp   DWORD [esi], 0x00
    jne   getuptime_loop06_top
  add   esp, 0x10

  atoui_end:
  ; close file
  mov   eax, 0x06
  mov   ebx, DWORD [ebp - 0x04]
  
  mov   esp, ebp
  pop   ebp
  ret


getshell:
  ; void getshell(char *buf)
  push  ebp
  mov   ebp, esp

  ; symlinkbuf[28]  -> [ebp - 0x30] - [ebp - 0x11]
  ; procbuf[12]     -> [ebp - 0x10] - [ebp - 0x05]
  ; procid          -> [ebp - 0x04]
  sub   esp, 0x30

  ; initialize buf to all zeros
  mov   esi, DWORD [ebp + 0x08]
  mov   edi, 0x00
  getshell_loop00_top:
    mov   BYTE [esi + edi], 0x00
    inc   edi
  cmp   edi, 0x10
  jle   getshell_loop00_top

  ; get parent pid
  mov    eax, 0x40
  int    0x80
  mov    DWORD [ebp - 0x04], eax
  ; convert pid to string
  lea   eax, [ebp - 0x10]
  push  eax
  push  DWORD [ebp - 0x04]
  call  uitoa
  add   esp, 0x08

  ; concatenate symlinkbuf
  mov   BYTE [ebp - 0x30], '/'
  mov   BYTE [ebp - 0x2f], 'p'
  mov   BYTE [ebp - 0x2e], 'r'
  mov   BYTE [ebp - 0x2d], 'o'
  mov   BYTE [ebp - 0x2c], 'c'
  mov   BYTE [ebp - 0x2b], '/'
  ; add procbuf
  lea   esi, [ebp - 0x10]
  lea   edi, [ebp - 0x2a]
  cld
  getshell_loop01_top:
    movsb
    cmp   BYTE [esi], 0
    jne   getshell_loop01_top
  ; concatenate symlinkbuf
  mov   BYTE [edi], '/'
  mov   BYTE [edi + 0x01], 'e'
  mov   BYTE [edi + 0x02], 'x'
  mov   BYTE [edi + 0x03], 'e'
  mov   BYTE [edi + 0x04], 0x00

  ; readlink(symlinkbuf, buf)
  mov   eax, 0x55
  lea   ebx, [ebp - 0x30]
  mov   ecx, DWORD [ebp + 0x08]
  mov   edx, 0x10
  int   0x80

  mov   esp, ebp
  pop   ebp
  ret


getos:
  ; void getos(char *buf)
  push  ebp
  mov   ebp, esp

  ; buf[128]        -> [ebp - 0x88]
  ; offset          -> [ebp - 0x08]
  ; file descriptor -> [ebp - 0x04]
  sub   esp, 0x88

  ; open /etc/os-release
  mov   eax, 0x05
  push  0x00        ; push NULL
  push  0x00657361  ; push "ase"
  push  0x656c6572  ; push "rele"
  push  0x2d736f2f  ; push "/os-"
  push  0x6374652f  ; push "/etc"
  mov   ebx, esp    ; ebx now has the address of "/etc/os-release"
  xor   ecx, ecx
  xor   edx, edx
  int   0x80
  mov   DWORD [ebp - 0x04], eax
  add   esp, 0x14

  ; read until you match character "P"
  getos_loop00_preamble:
  mov   DWORD [ebp - 0x08], 0x00
  lea   edi, [ebp - 0x88]
  getos_loop00_top:
  mov   esi, DWORD [ebp - 0x08]

    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [esi + edi]
    mov   edx, 0x01
    int   0x80

  inc   DWORD [ebp - 0x08]
  cmp   BYTE [esi + edi], 'P'
  je    getos_loop01_top
  jmp   getos_loop00_top

  ; see if next char is 'R'
  getos_loop01_top:
  mov   esi, DWORD [ebp - 0x08]

    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [esi + edi]
    mov   edx, 0x01
    int   0x80

  inc   DWORD [ebp - 0x08]
  cmp   BYTE [esi + edi], 'R'
  jne   getos_loop00_preamble

  ; it is R, now read to the '"'
  getos_loop02_top:
  mov   esi, DWORD [ebp - 0x08]

    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [esi + edi]
    mov   edx, 0x01
    int   0x80

  inc   DWORD [ebp - 0x08]
  cmp   BYTE [esi + edi], '"'
  jne   getos_loop02_top

  ; Read until the next '"' into buf, replace with null byte
  mov   DWORD [ebp - 0x08], 0x00
  mov   edi, DWORD [ebp + 0x08]
  getos_loop03_top:
  mov   esi, DWORD [ebp - 0x08]

    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [esi + edi]
    mov   edx, 0x01
    int   0x80

  inc   DWORD [ebp - 0x08]
  cmp   BYTE [esi + edi], '"'
  jne   getos_loop03_top
  mov   BYTE [esi + edi], 0x00

  ; close file
  mov   eax, 0x06
  mov   ebx, DWORD [ebp - 0x04]
  int   0x80

  mov   esp, ebp
  pop   ebp
  ret


getcpu:
  ; int getcpu(char *buf)
  push  ebp
  mov   ebp, esp

  mov   eax, 0x80000000
  cpuid

  cmp   eax, 0x80000004
  jl    getcpu_nobrandstring

  mov   eax, 0x80000002
  cpuid
  mov   esi, DWORD [ebp + 0x08]
  mov   DWORD [esi + 0x00], eax
  mov   DWORD [esi + 0x04], ebx
  mov   DWORD [esi + 0x08], ecx
  mov   DWORD [esi + 0x0c], edx

  mov   eax, 0x80000003
  cpuid
  mov   DWORD [esi + 0x10], eax
  mov   DWORD [esi + 0x14], ebx
  mov   DWORD [esi + 0x18], ecx
  mov   DWORD [esi + 0x1c], edx

  mov   eax, 0x80000004
  cpuid
  mov   DWORD [esi + 0x20], eax
  mov   DWORD [esi + 0x24], ebx
  mov   DWORD [esi + 0x28], ecx
  mov   DWORD [esi + 0x2c], edx

  mov   eax, 0x00
  jmp   getcpu_return
  getcpu_nobrandstring:
  mov   eax, 0xffffffff

  getcpu_return:
  mov   esp, ebp
  pop   ebp
  ret
  
parsecpu:
  ; void parsecpu(cpu_tmp, cpu_info)
  push  ebp
  mov   ebp, esp

  sub   esp, 0x08

  mov   DWORD [ebp - 0x04], 0x00
  mov   DWORD [ebp - 0x08], 0x00

  mov   esi, DWORD [ebp + 0x08]
  parsecpu_loop00_top:
  mov   edi, DWORD [ebp - 0x04]
    
    cmp   BYTE [esi + edi], 0x20
    jne   parsecpu_loop00_end

  inc   DWORD [ebp - 0x04]
  jmp   parsecpu_loop00_top
  parsecpu_loop00_end:

  mov   edi, DWORD [ebp + 0x0c]
  parsecpu_loop01_top:
  mov   eax, DWORD [ebp - 0x04]
  mov   ebx, DWORD [ebp - 0x08]

    mov   cl, BYTE[esi + eax]
    mov   BYTE[edi + ebx], cl

    cmp   cl, 0x00
    je    parsecpu_return
  
  inc   DWORD [ebp - 0x04]
  inc   DWORD [ebp - 0x08]
  jmp   parsecpu_loop01_top

  parsecpu_return:
  mov   esp, ebp
  pop   ebp
  ret


getuid:
  ; void getuser(char *buf)
  push  ebp
  mov   ebp, esp

  sub   esp, 0x04

  mov   eax, 0xc7
  int   0x80
  mov   DWORD [ebp - 0x04], eax

  push  DWORD [ebp + 0x08]
  push  DWORD [ebp - 0x04]
  call  uitoa
  add   esp, 0x08

  mov   esp, ebp
  pop   ebp
  ret

  
idtoname:
  ; void idtoname(char *uid, char *buf)
  ; NOTE: undefined behavior: tmp_buf is overflowed
  push  ebp
  mov   ebp, esp

  sub   esp, 0x94
  ;  id_buf[8]        -> [ebp - 0x94] - [ebp - 0x8d]
  ;  colons           -> [ebp - 0x8c]
  ;  tmp_buf[128]     -> [ebp - 0x88] - [ebp - 0x09]
  ;  offset           -> DWORD [ebp - 0x08]
  ;  file_descriptor  -> DWORD [ebp - 0x04]

  ; open /etc/passwd
  mov   eax, 0x05
  push  0x00        ; push NULL
  push  0x00647773  ; push "swd"
  push  0x7361702f  ; push "/pas"
  push  0x6374652f  ; push "/etc"
  mov   ebx, esp    ; ebx now has the address to "/etc/passwd"
  xor   ecx, ecx
  xor   edx, edx
  int   0x80
  mov   DWORD [ebp - 0x04], eax
  add   esp, 0x10

  ; read to second ':' character, store in buffer
  idtoname_loop01_preamble:
  mov   DWORD [ebp - 0x08], 0x00
  mov   DWORD [ebp - 0x8c], 0x00
  idtoname_loop01_top:
  mov   edi, DWORD [ebp - 0x08]
  lea   esi, [ebp - 0x88]
  
    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [edi + esi]
    mov   edx, 0x01
    int   0x80

    cmp   BYTE [edi + esi], ':'
    jne   idtoname_loop01_end
    inc   DWORD [ebp - 0x8c]
  

  idtoname_loop01_end:
  inc   DWORD [ebp - 0x08]
  cmp   DWORD [ebp - 0x8c], 0x02
  jl    idtoname_loop01_top

  ; read to next ':' character, store in separate buffer
  mov   DWORD [ebp - 0x08], 0x00
  idtoname_loop02_top:
  mov   edi, DWORD [ebp - 0x08]
  lea   esi, [ebp - 0x94]

    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [edi + esi]
    mov   edx, 0x01
    int   0x80

  inc   DWORD [ebp - 0x08]
  cmp   BYTE [edi + esi], ':'
  jne   idtoname_loop02_top
  ; quick replace ':' with null byte
  mov   BYTE [edi + esi], 0x00

  ; compare buf and id_buf to the null byte
  mov   DWORD [ebp - 0x08], 0x00
  idtoname_loop03_top:
  mov   edi, DWORD [ebp - 0x08]
  lea   eax, [ebp - 0x94]
  mov   ebx, DWORD [ebp + 0x8]

    mov   cl, BYTE [eax + edi]
    mov   dl, BYTE [ebx + edi]

    cmp   cl, dl
    jne   idtoname_loop04_preamble

  inc   DWORD [ebp - 0x08]
  cmp   cl, 0x00
  jne   idtoname_loop03_top
  jmp   idtoname_loop05_preamble

  ; they are not equal, read to NEWLINE and jmp idtoname_loop01_top
  idtoname_loop04_preamble:
  mov   DWORD [ebp - 0x08], 0x00
  idtoname_loop04_top:
  mov   edi, DWORD [ebp - 0x08]
  lea   esi, [ebp - 0x88]
    
    mov   eax, 0x03
    mov   ebx, DWORD [ebp - 0x04]
    lea   ecx, [edi + esi]
    mov   edx, 0x01
    int   0x80
  
    cmp   BYTE [edi + esi], 0x0a
    je    idtoname_loop01_preamble

  inc   DWORD [ebp - 0x08]
  jmp   idtoname_loop04_top

  ; they are equal, put the name in the tmp_buf -> buf
  idtoname_loop05_preamble:
  mov   DWORD [ebp - 0x08], 0x00
  xor   eax, eax
  idtoname_loop05_top:
  mov   edx, DWORD [ebp - 0x08]
  lea   esi, [ebp - 0x88]
  mov   edi, DWORD [ebp + 0x0c]

    mov   al, BYTE [esi + edx]
    mov   BYTE [edi + edx], al
  
    cmp   al, ':'
    jne   idtoname_loop05_end
    mov   BYTE [edi + edx], 0x00
    jmp   idtoname_done

  idtoname_loop05_end:
  inc   DWORD [ebp - 0x08]
  jmp   idtoname_loop05_top

  idtoname_done:
  ; close file
  mov   eax, 0x06
  mov   ebx, DWORD [ebp - 0x04]
  int   0x80

  mov   esp, ebp
  pop   ebp
  ret


uitoa:
  ; void uitoa(unsigned int i, char* buf)
  push  ebp
  mov   ebp, esp

  sub   esp, 0x0c
  ; int pops    -> [ebp - 0x0c]
  ; int n       -> [ebp - 0x08]
  ; int offset  -> [ebp - 0x04]
  ; int i       -> [ebp + 0x08]
  ; char *buf   -> [ebp + 0x0c]
  mov   DWORD [ebp - 0x0c], 0x00
  mov   DWORD [ebp - 0x04], 0x00
  mov   eax, DWORD [ebp + 0x08]
  mov   DWORD [ebp - 0x08], eax

  ; push digits onto the stack
  itoa_loop_top:
  mov   eax, DWORD [ebp - 0x08]
  mov   ebx, 0x0a
  mov   edx, 0x00
  idiv  ebx
  cmp   eax, 0x00
  je   itoa_loop_end

    push  edx
    mov   DWORD[ebp - 0x08], eax

  inc   DWORD [ebp - 0x0c]
  cmp   eax, 0x00
  jne   itoa_loop_top
  itoa_loop_end:
  push  edx
  inc   DWORD [ebp - 0x0c]

  ; pop off the stack, into char* buf
  itoa_loop1_top:
  cmp   DWORD[ebp - 0x0c], 0x00
  je    itoa_loop1_end

    mov   esi, [ebp + 0x0c]
    mov   edi, [ebp - 0x04]
    pop   eax
    add   al, 0x30
    mov   BYTE [esi + edi], al

  inc   DWORD [ebp - 0x04]
  dec   DWORD [ebp - 0x0c]
  jmp   itoa_loop1_top
  itoa_loop1_end:

  ; append null byte
  mov   esi, [ebp + 0x0c]
  mov   edi, [ebp - 0x04]
  mov   BYTE [esi + edi], 0x00

  mov   esp, ebp
  pop   ebp
  ret


printcat:
  ; void printcat(int n, ...)
  push  ebp
  mov   ebp, esp

  sub   esp, 0x04
  mov   DWORD [ebp - 0x04], 0x00

  printcat_loop_top:
  mov   esi, DWORD [ebp - 0x04]
  cmp   esi, DWORD [ebp + 0x08]
  je    printcat_loop_end

    shl   esi, 0x02
    add   esi, 0x0c

    push  DWORD [ebp + esi]
    call  puts
    add   esp, 0x04

  inc   DWORD [ebp - 0x04]
  jmp   printcat_loop_top
  printcat_loop_end:

  mov   esp, ebp
  pop   ebp
  ret
  

puts:
  ; void puts(char *buf)
  push  ebp
  mov   ebp, esp

  ; length -> [ebp - 0x04]
  sub   esp, 0x04

  ; length = strlen(buf)
  push  DWORD [ebp + 0x08]
  call  strlen
  add   esp, 0x04
  mov   DWORD [ebp - 0x04], eax

  ; write(FILENO_STDOUT, buf, length)
  mov   eax, 0x04
  mov   ebx, 0x01
  mov   ecx, DWORD [ebp + 0x08]
  mov   edx, DWORD [ebp - 0x04]
  int   0x80

  mov   esp, ebp
  pop   ebp
  ret


strlen:
  ; unsigned int strlen(char *buf)
  push  ebp
  mov   ebp, esp

  mov   esi, DWORD [ebp + 0x08]
  mov   eax, 0x00
  strlen_toploop:
  cmp   BYTE [esi + eax], 0x00
  je    strlen_endloop
  inc   eax
  jmp   strlen_toploop
  strlen_endloop:

  mov   esp, ebp
  pop   ebp
  ret
  

uname:
  ; void uname(struct utsname *buf)
  push  ebp
  mov   ebp, esp

  mov   eax, 0x6d
  mov   ebx, DWORD [ebp + 0x08]
  int   0x80

  mov   esp, ebp
  pop   ebp
  ret


strncmp:
  ; int strncmp(char *str1, char *str2, size_t s)
  push  ebp
  mov   ebp, esp
  
  xor   eax, eax
  xor   ebx, ebx
  xor   ecx, ecx
  mov   esi, DWORD [ebp + 0x08]
  mov   edi, DWORD [ebp + 0x0c]
  strncmp_loop00_top:
  cmp   ecx, DWORD [ebp + 0x10]
  je    strncmp_done

    mov   bl, BYTE [esi + ecx]
    mov   dl, BYTE [edi + ecx]
    sub   dl, bl

  add   eax, edx
  inc   ecx
  jmp   strncmp_loop00_top
  strncmp_done:

  mov   esp, ebp
  pop   ebp
  ret


_start:
  push  ebp
  mov   ebp, esp

  ; get random eyes
  push  0x0c
  push  0x00
  call  getrandom
  add   esp, 0x08
  shl   eax, 2
  mov   DWORD [eyeindex], eax

  ; uname(utsname)
  ; grabs sysname, nodename, release, version, machine
  push  utsname
  call  uname
  add   esp, 0x04

  ; getuid(uid)
  ; grabs uid in string format
  push  uid
  call  getuid
  add   esp, 0x04

  ; idtoname(uid, username)
  ; converts uid to username
  push  username
  push  uid
  call  idtoname
  add   esp, 0x08
  
  ; getcpu(cpu_info)
  push  cpu_tmp
  call  getcpu
  add   esp, 0x04
  ; parsecpu(cpu_tmp, cpu_info)
  push  cpu_info
  push  cpu_tmp
  call  parsecpu
  add   esp, 0x08

  ; getos(os)
  push  os
  call  getos
  add   esp, 0x04

  ; getshell(shell)
  push  shell
  call  getshell
  add   esp, 0x04

  ; getuptime
  push  uptime
  call  getuptime
  add   esp, 0x04

  ; check if argument is present
  cmp   DWORD [esp + 0x04], 0x02
  jl    print_ascii

  ; strcmp(no_ascii, argv[1], 10)
  mov   eax, DWORD [esp + 0x0c]
  push  0x0a
  push  eax
  push  no_ascii
  call  strncmp
  add   esp, 0x0c

  cmp   eax, 0x00
  je    print_noascii

  ; basename(argv[0], filename)
  push  filename
  push  DWORD [ebp + 0x08]
  call  basename
  add   esp, 0x08

  ; print help
  push  help_out02
  push  help_out01
  push  filename
  push  help_out00
  push  0x04
  call  printcat
  add   esp, 0x10

  jmp   finale


  ; Print->ascii
  print_ascii:
    push  NEWLINE
    push  ANSI_RESET
    push  nodename
    push  at_sign
    push  username
    push  ANSI_BLUE
    push  ANSI_ITALIC
    push  ANSI_BOLD
    push  bunny_00
    push  0x09
    call  printcat
    add   esp, 0x28

    push  NEWLINE
    push  os
    push  os_out00
    push  bunny_00
    push  0x04
    call  printcat
    add   esp, 0x14

    push  NEWLINE
    push  release
    push  utsname_out02
    push  bunny_01
    push  0x04
    call  printcat
    add   esp, 0x14

    push  NEWLINE
    push  cpu_info
    push  cpuinfo_out00
    push  bunny_03
    lea   esi, [eyes]
    mov   edi, DWORD [eyeindex]
    add   esi, edi
    push  esi
    push  bunny_02
    push  0x06
    call  printcat
    add   esp, 0x1c

    push  NEWLINE
    push  shell
    push  shell_out00
    push  bunny_04
    push  0x04
    call  printcat
    add   esp, 0x14

    push  NEWLINE
    push  uptime
    push  uptime_out00
    push  bunny_00
    push  0x04
    call  printcat
    add   esp, 0x14

    push  NEWLINE
    push  0x01
    call  printcat

    jmp   finale

  ; Print->no-ascii
  print_noascii:
    push  NEWLINE
    push  ANSI_RESET
    push  nodename
    push  at_sign
    push  username
    push  ANSI_BLUE
    push  ANSI_ITALIC
    push  ANSI_BOLD
    push  0x08
    call  printcat
    add   esp, 0x24

    push  NEWLINE
    push  os
    push  os_out00
    push  0x03
    call  printcat
    add   esp, 0x10

    push  NEWLINE
    push  release
    push  utsname_out02
    push  0x03
    call  printcat
    add   esp, 0x10

    push  NEWLINE
    push  cpu_info
    push  cpuinfo_out00
    push  0x03
    call  printcat
    add   esp, 0x10

    push  NEWLINE
    push  shell
    push  shell_out00
    push  0x03
    call  printcat
    add   esp, 0x10

    push  NEWLINE
    push  uptime
    push  uptime_out00
    push  0x03
    call  printcat
    add   esp, 0x10

    push  NEWLINE
    push  0x01
    call  printcat
    add   esp, 0x08


  finale:
  ; exit(0)
  mov    eax, 0x01
  xor    ebx, ebx
  int    0x80
