# afetch
<img alt="example output of afetch" src="example.png" width="50%">

A simple info fetch tool written in x86 assembly.

## Installation:
Clone the git repository.

`git clone https://gitlab.com/tomasbulk/afetch.git`

Enter the cloned repositority.

`cd afetch`

Use `make`.

`make`

## Usage:
```bash
Usage: afetch [--no-ascii]
	--no-ascii      Omit ascii art.
```

### Other info:
Below are some notes taken:
- username - using the getuid system call, we can obtain the numerical user ID, from which we can open and read /etc/passwd to obtain the corresponding username.
- hostname - gathered using the uname system call
- os - obtained by reading /etc/os-release and reading the variable $PRETTY_NAME
- kernel version - gathered using the uname system call
- cpu - using the cpuid instruction, we can get a vendor-made ascii string describing the cpu
- uptime - by reading the number of seconds in /proc/uptime, we can calculate how many days/hours/minutes the system has been up
- shell - afetch determines the current running shell by using the getppid to find the process ID of the whatever called the script (ie. the shell), and looks in /proc/$ID/exe to read the symlink, and grab the current shell
- Other features include:
  - A cute bunny ascii art with random eyes chosen every time.
  - A --no-ascii option that only displays system information
  - Prints a usage guide if you input any other parameter other than --no-ascii
