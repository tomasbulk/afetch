all: afetch

afetch : afetch.o
	ld -m elf_i386 -o $@ $^

afetch.o : afetch.asm
	nasm -g -f elf32 -o $@ $^

.PHONY : clean

clean : afetch.o afetch
	@rm -rf $^
